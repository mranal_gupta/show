import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    loadChildren: "src/app/details/details.module#DetailsModule"
  },
  {
    path: "details",
    loadChildren: "src/app/details/details.module#DetailsModule"
  },
  {
    path: "student",
    loadChildren: "src/app/student/student.module#StudentModule"
  },
  {
    path: "courses",
    loadChildren: "src/app/courses/courses.module#CoursesModule"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

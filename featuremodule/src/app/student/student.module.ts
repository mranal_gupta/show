import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StudentsComponent } from "./students/students.component";
import { StudentRoutingModule } from "./student-routing.module";

@NgModule({
  declarations: [StudentsComponent],
  imports: [CommonModule, StudentRoutingModule],
  exports: [StudentsComponent]
})
export class StudentModule {
  constructor() {
    console.log("Student Module is wokring   ");
  }
}

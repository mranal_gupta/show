const express = require("express");
const bodyParser = require('body-parser');
const app = express();

app.use("/abc", express.static("public"));
app.set("view engine", "twig");
app.set("views", "./public/Views");

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get("/", (req, res) => {
  res.render("index", {
    title: "Login Form",
    message: "Enter UserName & Password"
  });
});

app.post("/", (req, res) => {
  res.render("login", {
    title: "User Details",
    username: req.body.username , 
    password:  req.body.password
  });
});



// app.get("/about/:a-:b", (req, res) => {
//   res.render("index", {
//     title: "Tutorials website",
//     message: parseInt(req.params.a) + parseInt(req.params.b)
//   });
// });

// var validation = function(req, res, next) {
//   if (req.params.username == "Pradeep") console.log("User Validation");
//   else console.log("not auth");
//   next();
// };

// app.use(validation);

// app.get("/user/:username", validation, (req, res) => {
//   res.send("user page");
// });

// app.get("/", (req, res) => {
//   res.send("Hellow world");
// });

// app.get("/", (req, res) => {
//   res.sendFile(__dirname + "/index.html");
// });

// app.get("/users/:Id?", (req, res) => {
//   if (req.params.Id == undefined) res.send("Data not accessed");
//   else res.send("Users Data Acccessed" + req.params.Id);
// });

// app.get("/flight/:Form?.:To?", (req, res) => {
//   res.send(
//     "Search for flight" + "Form:" + req.params.Form + "To:" + req.params.To
//   );
// });

app.listen(2000, () => console.log("Server Running on port 6000"));

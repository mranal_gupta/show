//Private Route
const router = require("express").Router();
const verify = require("./verifyToken");

router.get("/", verify, (req, res) => {
  //   res.send(req.user);
  res.json({
    posts: {
      title: "My First Post",
      description: "Random Data you should access "
    }
  });
});

module.exports = router;

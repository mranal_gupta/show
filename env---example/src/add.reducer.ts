import { Action, createAction, on } from "@ngrx/store";
import { Adding } from "./action.add";

export interface add {
  num1: number;
  num2: number;
}

export const initialState: add = {
    num1: 0 ,
    num2: 0
} 

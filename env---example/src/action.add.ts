import { createAction, props } from "@ngrx/store";

export const Adding = createAction(
  "[Add Page] Add ",
  props<{ num1: number; num2: number }>()
);

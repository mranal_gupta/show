import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { ActivateGuard } from './activate.guard';


const routes: Routes = [
  {
    path:'',component:HomeComponent
  },
  {
    path:'Home',component:HomeComponent
  },
  {
    path:'admin',component:AdminComponent,canActivate:[ActivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

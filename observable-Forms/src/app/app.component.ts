import { Component } from "@angular/core";
import { BookService } from "./book.service";
import { Book } from "./book";
import { Observable } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "observable";
  softBooks: Observable<Book>;
  constructor(private bookservice: BookService) {}

  ngOnInit() {
    this.getsoftBooks();
  }

  getsoftBooks() {
    this.softBooks = this.bookservice.getBooksFormStore(100);
  }
}
